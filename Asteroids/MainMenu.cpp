#include "MainMenu.h"
#include "System.h"
#include "Graphics.h"
#include "Game.h"
#include "FontEngine.h"
#include "Keyboard.h"
#include "Mouse.h"
#include <algorithm>
#include <list>

MainMenu::MainMenu()
{
}

MainMenu::~MainMenu()
{
}

void MainMenu::OnActivate(System *system, StateArgumentMap &args)
{
	std::list<int> scoreList = system->GetGame()->GetScoreList();

	//sort it, decending
	scoreList.sort( [](const int &x, const int &y) {
		return x < y;
	});
	

	if (scoreList.size() > 0)
	{
		for (int score : scoreList)
		{
			scores_.push_back(std::to_string(score));
		}
	}
}

void MainMenu::OnUpdate(System *system)
{
	Sleep(1);

	Keyboard *keyboard = system->GetKeyboard();
	if (keyboard->IsKeyPressed(VK_SPACE))
	{
		GameState::StateArgumentMap args;
		args["Level"].asInt = 1;
		args["Life"].asInt = 3;
		system->SetNextState("LevelStart", args);
	}
	else if (system->GetKeyboard()->IsKeyPressed(VK_ESCAPE))
		system->Quit();
}

void MainMenu::OnRender(System *system)
{
	Graphics *graphics = system->GetGraphics();
	FontEngine *fontEngine = graphics->GetFontEngine();

	system->GetGame()->RenderBackgroundOnly(graphics);

	fontEngine->DrawText("ASTEROIDS", 50, 50, 0xff00ffff, FontEngine::FONT_TYPE_LARGE);
	fontEngine->DrawText("Press [Space] to Start", 50, 120, 0xffffffff, FontEngine::FONT_TYPE_MEDIUM);
	
	//display score table
	if (scores_.size() > 0)
	{
		fontEngine->DrawText("Score List", 500, 50, 0xffffffff, FontEngine::FONT_TYPE_MEDIUM);


		int currentLineY = 70;
		for (StringList::iterator stringIt = scores_.begin(), end = scores_.end();
			stringIt != end;
			++stringIt)
		{
			currentLineY += fontEngine->DrawText(*stringIt, 550, currentLineY, 0xffffffff, FontEngine::FONT_TYPE_SMALL);
		}
	}
	else
	{
		fontEngine->DrawText("No Scores Yet !!!", 500, 100, 0xffffffff, FontEngine::FONT_TYPE_MEDIUM);
	}


	fontEngine->DrawText("Controls", 100, 200, 0xffffffff, FontEngine::FONT_TYPE_MEDIUM);
	fontEngine->DrawText("Movement : W,A,S,D / UP,LEFT,DOWN,RIGHT / RIGHT MOUSE BUTTON", 100, 300, 0xffffffff, FontEngine::FONT_TYPE_SMALL);
	fontEngine->DrawText("Shoot : SPACE / LEFT MOUSE BUTTON", 100, 350, 0xffffffff, FontEngine::FONT_TYPE_SMALL);
	fontEngine->DrawText("GameOver : ESCAPE DURING GAME", 100, 400, 0xffffffff, FontEngine::FONT_TYPE_SMALL);
	fontEngine->DrawText("NextLevel : SHIFT DURING GAME", 100, 450, 0xffffffff, FontEngine::FONT_TYPE_SMALL);
	fontEngine->DrawText("Exit : ESCAPE DURING MENU", 100, 500, 0xffffffff, FontEngine::FONT_TYPE_SMALL);
	
}

void MainMenu::OnDeactivate(System *system)
{
}
