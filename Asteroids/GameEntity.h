#ifndef GAMEENTITY_H_INCLUDED
#define GAMEENTITY_H_INCLUDED

#include <DirectXMath.h>

using namespace DirectX;

class System;
class Graphics;
class Collision;
class Collider;

class GameEntity
{
public:
	GameEntity(int bitsetVal);
	virtual ~GameEntity();

	virtual void Update(System *system);
	virtual void Render(Graphics *graphics);

	XMVECTOR GetPosition() const;
	void SetPosition(XMVECTOR position);

	int GetRadius() const;

	void EnableCollisions(Collision *collisionSystem, float radius);
	void DisableCollisions();

	void MarkForDelete();
	bool IsMarkedForDelete();

	void SetDebug(bool debug);
	bool IsDebug();

	unsigned int GetId();

	int radius_;

	Collider *GetCollider();

	static unsigned int GameEntityCount;
private:

	bool HasValidCollider() const;
	void DestroyCollider();

	XMFLOAT3 position_;
	Collision *collisionSystem_;
	Collider *collider_;
	bool markDelete;
	int bitsetValue_;
	bool debug_;
	unsigned int id_;
};

#endif // GAMEENTITY_H_INCLUDED
