#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#include <DirectXMath.h>
#include <list>

using namespace DirectX;

class OrthoCamera;
class Background;
class Ship;
class Bullet;
class Asteroid;
class Explosion;
class Collision;
class System;
class Graphics;
class GameEntity;
class UFO;
class Collider;

class Game
{
public:
	Game();
	~Game();

	void Update(System *system);
	void RenderBackgroundOnly(Graphics *graphics);
	void RenderEverything(Graphics *graphics);

	void InitialiseLevel(int numAsteroids, int life);
	bool IsLevelComplete() const;
	bool IsGameOver() const;

	void DoCollision(GameEntity *a, GameEntity *b);
	
	int GetScore();
	int GetLife();
	int GetCollisionCount();
	void IncrementCollisionCount();

	int GetBulletLevel();

	void SaveScore();
	std::list<int> GetScoreList();

	void PrintVec(char *prefix, XMVECTOR const &vec);

	bool IsDebugView();
	Collider *GetPlayerCollider();
	std::list<Collider*> GetBulletColliders();

private:
	Game(const Game &);
	void operator=(const Game &);

	typedef std::list<Bullet *> BulletList;
	typedef std::list<Asteroid *> AsteroidList;
	typedef std::list<Explosion *> ExplosionList;
	typedef std::list<int> ScoreList;

	void SpawnPlayer();	
	void DeletePlayer();

	void SpawnUFOAt(XMVECTOR pos);
	void DeleteUFO();

	void UpdateSystems(System *system);
	void UpdatePlayer(System *system);
	void UpdateUFO(System *system);
	void UpdateAsteroids(System *system);
	void UpdateBullet(System *system);
	void UpdateExplosions(System *system);
	void WrapEntity(GameEntity *entity) const;

	void DeleteAllBullets();
	void DeleteAllAsteroids();
	void DeleteAllExplosions();

	void SpawnExplosion(XMVECTOR position, int lifeTime);

	void SpawnBulletBasedOnPlayer();
	void SpawnBullet(XMVECTOR position, XMVECTOR direction, int lifeTime);
	void DeleteBullet(Bullet *bullet);
	void DeleteExplosion(Explosion * explosion);
	bool IsBullet(GameEntity *entity) const;

	void SpawnAsteroids(int numAsteroids);
	void SpawnAsteroidAt(XMVECTOR position, int size);
	bool IsAsteroid(GameEntity *entity) const;
	void AsteroidHit(Asteroid *asteroid);
	void DeleteAsteroid(Asteroid *asteroid);

	void UpdateCollisions();
	void CleanUpEntities();

	OrthoCamera *camera_;

	Background *background_;
	Ship *player_;
	UFO *ufo_;
	AsteroidList asteroids_;
	ExplosionList explosions_;
	BulletList bullets_;
	ScoreList gameScores_;

	Collision *collision_;
	int bulletLifeTime_;
	int score_;
	int life_;
	int spawnBulletDelay_;
	int spawnBulletDelayLimit_;
	int playerTurnTick_;
	int bulletLevel_;
	int toggleDebugView_;
	int collisionCheckCount_;
};

#endif // GAME_H_INCLUDED
