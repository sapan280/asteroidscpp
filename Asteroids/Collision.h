#ifndef COLLISION_H_INCLUDED
#define COLLISION_H_INCLUDED

#include <DirectXMath.h>
#include <list>

using namespace DirectX;

class GameEntity;
class Game;
class Collider;
class SpatialHash;
class Graphics;

class Collision
{
public:
	Collision();
	~Collision();

	Collider *CreateCollider(GameEntity *entity, int setvalue);
	void DestroyCollider(Collider *collider);

	void UpdateColliderPosition(Collider *collider, const XMFLOAT3 &position);
	void UpdateColliderRadius(Collider *collider, float radius);
	void EnableCollider(Collider *collider);
	void DisableCollider(Collider *collider);

	void DoCollisions(Game *game);

	void RenderSpatialHash(Graphics *graphics);
	void RenderCell(Graphics *graphics, Collider *collider, uint32_t color);

private:

	typedef std::list<Collider *> ColliderList;

	bool CollisionTest(Collider *a, Collider *b);
	void CheckCollisionsWithNeighbours(Collider *collider, Game *game);

	ColliderList colliders_;
	SpatialHash *spatialHash_;
};

#endif // COLLISION_H_INCLUDED
