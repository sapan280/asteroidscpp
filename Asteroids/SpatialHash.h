#ifndef SPATIALHASH_H_INCLUDED
#define SPATIALHASH_H_INCLUDED

#include <map>
#include <list>
#include "ImmediateModeVertex.h"

class Collider;

struct Vec2f
{
	float x;
	float y;

	Vec2f(float x_, float y_)
	{
		x = x_;
		y = y_;
	}
};

struct CellLoc
{
	int x;
	int y;

	CellLoc(int x_, int y_)
	{
		x = x_;
		y = y_;
	}

	bool operator <(const CellLoc& rhs) const
	{
		return x < rhs.x && x < rhs.y;
	}

	bool operator >(const CellLoc& rhs) const
	{
		return x > rhs.x && x > rhs.y;
	}

	bool operator ==(const CellLoc& rhs) const
	{
		return x == rhs.x && y == rhs.y;
	}
};

class Graphics;
class ImmediateMode;

class SpatialHash
{
private:
	typedef std::map<CellLoc, std::list<Collider*>> BucketMap;

	typedef std::list<int> BucketIDList;

	BucketMap bucketMap_;
	int screenWidth_;
	int screenHeight_;
	int cellSize_;
	int columns_;
	int rows_;

	BucketIDList bucketItemList_;

	void AddBucket(Vec2f vec, int width, std::list<CellLoc> &bucketToAdd);
public:
	SpatialHash(int width, int height, int size);
	~SpatialHash();

	void Update();
	void Render(Graphics *graphics);
	void RenderCell(ImmediateMode *mode, int x, int y, uint32_t color);

	std::list<CellLoc> GetIdForObj(Collider *entity);
	void InsertEntity(Collider *entity);

	std::list<Collider*> FetchNearbyEntity(Collider *entity, unsigned int sourceId);

	enum { NUM_LINES = 5 };

	ImmediateModeVertex lines_[NUM_LINES];

	void DeleteAll();

	int GetCellSize();
};

#endif //SPATIALHASH_H_INCLUDED