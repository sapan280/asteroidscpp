#include "SpatialHash.h"

#include "GameEntity.h"
#include "Collider.h"
#include "Graphics.h"
#include "ImmediateMode.h"
#include "ImmediateModeVertex.h"
#include <DirectXMath.h>
#include "Random.h"
#include <algorithm>

void SpatialHash::AddBucket(Vec2f vec, int width, std::list<CellLoc>& bucketToAdd)
{
	CellLoc cellPosition{ (int)floor(vec.x / cellSize_) , (int)floor(vec.y / cellSize_) };

	if (!(std::find(bucketToAdd.begin(), bucketToAdd.end(), cellPosition) != bucketToAdd.end()))
	{
		//char s[256];
		//sprintf_s(s, "AddBucket -> [ x %d y %d ]\n", cellPosition.x, cellPosition.y);
		//OutputDebugStringA(s);

		bucketToAdd.push_back(cellPosition);
	}
}

SpatialHash::SpatialHash(int width, int height, int size)
{
	screenWidth_ = width;
	screenHeight_ = height;
	cellSize_ = size;
	columns_ = screenWidth_ / cellSize_;
	rows_ = screenHeight_ / cellSize_;

	for (int i = 0; i < columns_ * rows_; i++)
	{
		//bucketMap_.insert(std::pair<Vec2, std::list<Collider*>>(Vec2, std::list<Collider*>()));
	}

	float halfWidth = 800 * 0.5f;
	float halfHeight = 600 * 0.5f;

	float radius_ = (float)cellSize_;

	lines_[0] = { 0.0f, 0.0f, 0.0f, 0x007e7e7e };
	lines_[1] = { 0.0f,  radius_, 0.0f, 0x007e7e7e };
	lines_[2] = { radius_,  radius_, 0.0f, 0x007e7e7e };
	lines_[3] = { radius_, 0.0f, 0.0f, 0x007e7e7e };
	lines_[4] = { 0.0f, 0.0f, 0.0f, 0x007e7e7e };
}


SpatialHash::~SpatialHash()
{

}

void SpatialHash::Update()
{

}

void SpatialHash::Render(Graphics *graphics)
{
	ImmediateMode *immediateGraphics = graphics->GetImmediateMode();
	for (int c = -columns_/2; c < columns_/2; c++)
	{
		for (int r = -rows_/2; r < rows_ /2; r++)
		{
			/*char s[256];
			sprintf_s(s, "c %d r %d\n", c, r);
			OutputDebugStringA(s);*/


			RenderCell(immediateGraphics, c, r, 0x007e7e7e);
		}
	}

}

void SpatialHash::RenderCell(ImmediateMode * mode, int x, int y, uint32_t color)
{
	XMVECTOR position = XMVectorSet((float)cellSize_ * x, (float)cellSize_ * y + 1, 0.0f, 0.0f);

	XMMATRIX translationMatrix = XMMatrixTranslation(
		XMVectorGetX(position),
		XMVectorGetY(position),
		XMVectorGetZ(position));

	mode->SetModelMatrix(translationMatrix);

	for (int i = 0; i < NUM_LINES; i++)
		lines_[i].diffuse = color;

	mode->Draw(D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP,
		&lines_[0],
		5);

	mode->SetModelMatrix(XMMatrixIdentity());
}


std::list<CellLoc> SpatialHash::GetIdForObj(Collider *entity)
{
	std::list<CellLoc> bucketObjectsInList;

	float radius = entity->radius/2;

	//XMFLOAT3 pos = XMFLOAT3( entity->position.x - 400, entity->position.y, 0.0f);

	XMFLOAT3 pos = entity->position;

	//origin at center
	Vec2f min( pos.x - radius, pos.y - radius);
	Vec2f max( pos.x + radius, pos.y + radius);

	int width = screenWidth_ / cellSize_;

	AddBucket(min, width, bucketObjectsInList);
	AddBucket(Vec2f(max.x, min.y), width, bucketObjectsInList);
	AddBucket(Vec2f(max.x, max.y), width, bucketObjectsInList);
	AddBucket(Vec2f(min.x, max.y), width, bucketObjectsInList);

	return bucketObjectsInList;
}

void SpatialHash::InsertEntity(Collider * entity)
{
	std::list<CellLoc> cellIds = GetIdForObj(entity);
	
	for (CellLoc i : cellIds)
	{
		//char s[256];
		//sprintf_s(s, "save -> [ x %d, y %d ]\n", i.x, i.y);

		//OutputDebugStringA(s);

		bucketMap_[i].push_back(entity);
	}
}

std::list<Collider*> SpatialHash::FetchNearbyEntity(Collider * entity, unsigned int sourceId)
{
	std::list<Collider*> itemList;
	std::list<CellLoc> objIDs = GetIdForObj(entity);
	
	for (CellLoc id : objIDs)
	{
		for (auto a : bucketMap_[id])
		{
			if (a->entity->GetId() == sourceId)
				continue;

			itemList.push_back(a);
		}
	}

	return itemList;
}

void SpatialHash::DeleteAll()
{
	bucketMap_.clear();
}

int SpatialHash::GetCellSize()
{
	return cellSize_;
}
