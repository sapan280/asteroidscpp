#include "PlayingState.h"
#include "System.h"
#include "Graphics.h"
#include "Game.h"
#include "FontEngine.h"
#include "Keyboard.h"
#include "Mouse.h"

PlayingState::PlayingState()
{
}

PlayingState::~PlayingState()
{
}

void PlayingState::OnActivate(System *system, StateArgumentMap &args)
{
	Game *game = system->GetGame();

	level_ = args["Level"].asInt;
	life_ = args["Life"].asInt;
	game->InitialiseLevel(level_, life_);
}

void PlayingState::OnUpdate(System *system)
{
	Game *game = system->GetGame();
	game->Update(system);
	if (game->IsLevelComplete() || system->GetKeyboard()->IsKeyPressed(VK_SHIFT))
	{
		game->SaveScore();

		StateArgumentMap args;
		args["Level"].asInt = level_ + 1;
		args["Life"].asInt = life_;
		system->SetNextState("LevelStart", args);
	}
	else if (game->IsGameOver() || system->GetKeyboard()->IsKeyPressed(VK_ESCAPE))
	{
		game->SaveScore();

		system->SetNextState("GameOver");
	}

}

void PlayingState::OnRender(System *system)
{
	Graphics *graphics = system->GetGraphics();
	FontEngine *fontEngine = graphics->GetFontEngine();

	Game *game = system->GetGame();
	game->RenderEverything(system->GetGraphics());

	char gameData[256];


	sprintf_s(gameData, "Score : %d", game->GetScore());
	fontEngine->DrawText(gameData, 10, 10, 0xffffffff, FontEngine::FONT_TYPE_SMALL);

	sprintf_s(gameData, "Life : %d", game->GetLife());
	fontEngine->DrawText(gameData, 110, 10, 0xffffffff, FontEngine::FONT_TYPE_SMALL);

	sprintf_s(gameData, "F1 (toggle debug view)", game->GetLife());
	fontEngine->DrawText(gameData, 210, 10, 0xffffffff, FontEngine::FONT_TYPE_SMALL);

	sprintf_s(gameData, "Bullet Level : %d ( + key / - key )", game->GetBulletLevel());
	fontEngine->DrawText(gameData, 410, 10, 0xffffffff, FontEngine::FONT_TYPE_SMALL);

	if (game->IsDebugView())
	{
		//any debug data which has to be shown	

		sprintf_s(gameData, "Collision Count : %d", game->GetCollisionCount());
		fontEngine->DrawText(gameData, 10, 35, 0xffffffff, FontEngine::FONT_TYPE_SMALL);

	}
}

void PlayingState::OnDeactivate(System *system)
{
}
