#include "Explosion.h"
#include "Graphics.h"
#include "ImmediateMode.h"
#include "ImmediateModeVertex.h"

Explosion::Explosion(XMVECTOR position, int size)
	:GameEntity(6)
{
	radius_ = size;
	lifeTime_ = size * 2;
	SetPosition(position);
}

void Explosion::Update(System *system)
{
	lifeTime_--;

	if (lifeTime_ <= 0)
		MarkForDelete();

	radius_++;	//the size keeps on increasing
}

void Explosion::Render(Graphics *graphics)
{
	ImmediateModeVertex square[5] =
	{
		{-1.0f, -1.0f, 0.0f, 0xff0000ff},
		{-1.0f,  1.0f, 0.0f, 0xff0000ff},
		{ 1.0f,  1.0f, 0.0f, 0xff0000ff},
		{ 1.0f, -1.0f, 0.0f, 0xff0000ff},
		{-1.0f, -1.0f, 0.0f, 0xff0000ff},
	};

	XMMATRIX scaleMatrix = XMMatrixScaling(
		radius_,
		radius_,
		radius_
		);

	XMVECTOR position = GetPosition();
	XMMATRIX translationMatrix = XMMatrixTranslation(
		XMVectorGetX(position),
		XMVectorGetY(position),
		XMVectorGetZ(position));

	XMMATRIX asteroidTransform = scaleMatrix  * translationMatrix;
	ImmediateMode *immediateGraphics = graphics->GetImmediateMode();

	immediateGraphics->SetModelMatrix(asteroidTransform);
	immediateGraphics->Draw(D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP,
		&square[0],
		5);
	immediateGraphics->SetModelMatrix(XMMatrixIdentity());
}