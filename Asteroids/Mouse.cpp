#include "Mouse.h"
#include "System.h"
#include "MainWindow.h"


Mouse::Mouse()
{
}


Mouse::~Mouse()
{
}

void Mouse::Update(System *system)
{
	GetCursorPos(&p);
	ScreenToClient(system->GetMainWindow()->GetHandle(), &p);
	p.x -= 400;	//transform by center of screen
	p.y -= 300;	//transform by center of screen
	p.y *= -1;	//invert y
}

POINT Mouse::GetPointAfterTransform()
{
	return p;
}

bool Mouse::IsLeftMouseButtonDown()
{
	return ((GetKeyState(VK_LBUTTON) & 0x100) != 0);
}

bool Mouse::IsRightMouseButtonDown()
{
	return ((GetKeyState(VK_RBUTTON) & 0x100) != 0);
}
