#include "System.h"
#include "MainWindow.h"
#include "ResourceLoader.h"
#include "Graphics.h"
#include "AssetLoader.h"
#include "StateLibrary.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "GameState.h"
#include "Game.h"
#include "Common.h"
#include <chrono>

System::System(HINSTANCE hInstance) :
	moduleInstance_(hInstance),
	mainWindow_(0),
	quit_(false),
	resourceLoader_(0),
	graphics_(0),
	assetLoader_(0),
	stateLibrary_(0),
	keyboard_(0),
	mouse_(0),
	currentState_(0),
	nextState_(0),
	game_(0),
	frameCount_(0),
	accumulatedFrameTime_(0.0f),
	fps_(0),
	toggleDebugView_(false)
{
}

System::~System()
{
}

void System::Initialise()
{
	mainWindow_ = new MainWindow(moduleInstance_);
	resourceLoader_ = new ResourceLoader();
	graphics_ = Graphics::CreateDevice(mainWindow_->GetHandle(), resourceLoader_);
	assetLoader_ = new AssetLoader();
	stateLibrary_ = new StateLibrary();
	keyboard_ = new Keyboard();
	mouse_ = new Mouse();
	game_ = new Game();
}

void System::Test()
{
}

void System::Run()
{
	while (!quit_)
	{
		std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

		ProcessMessageQueue();
		SwapState();
		Update();
		Render();

		std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();

		std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);

		accumulatedFrameTime_ += time_span.count();

		if (accumulatedFrameTime_ < 1)
		{
			frameCount_++;
		}
		else
		{
			fps_ = 1/(accumulatedFrameTime_ / frameCount_);	//we could average over multiple frame to get a more stable FPS
			frameCount_ = 0;
			accumulatedFrameTime_ = 0.0f;
		}

		
		char titleWithFPS[256];
		sprintf_s(titleWithFPS, "Sumo Asteroids Test - Time : %f, Accumulated : %f, Basic FPS %d\n", time_span.count(), accumulatedFrameTime_, fps_);
		//OutputDebugStringA(titleWithFPS);
		GetMainWindow()->SetWindowTitle(titleWithFPS);

	}

	if (currentState_ != 0)
	{
		currentState_->OnDeactivate(this);
	}
}

void System::Terminate()
{
	SAFE_DELETE_PTR( game_ );

	SAFE_DELETE_PTR(keyboard_ );

	SAFE_DELETE_PTR(mouse_);

	SAFE_DELETE_PTR(stateLibrary_);

	SAFE_DELETE_PTR(assetLoader_);

	Graphics::DestroyDevice(graphics_);

	SAFE_DELETE_PTR(resourceLoader_);

	SAFE_DELETE_PTR(mainWindow_);
}

ResourceLoader *System::GetResourceLoader() const
{
	return resourceLoader_;
}

Graphics *System::GetGraphics() const
{
	return graphics_;
}

AssetLoader *System::GetAssetLoader() const
{
	return assetLoader_;
}

Keyboard *System::GetKeyboard() const
{
	return keyboard_;
}

Mouse *System::GetMouse() const
{
	return mouse_;
}

Game *System::GetGame() const
{
	return game_;
}

MainWindow * System::GetMainWindow() const
{
	return mainWindow_;
}

void System::SetNextState(const std::string &stateName)
{
	nextState_ = stateLibrary_->GetState(stateName);
	nextStateArgs_.clear();
}

void System::SetNextState(const std::string &stateName, const GameState::StateArgumentMap &args)
{
	nextState_ = stateLibrary_->GetState(stateName);
	nextStateArgs_ = args;
}

void System::Quit()
{
	quit_ = true;
}

void System::ProcessMessageQueue()
{
	MSG message;
	ZeroMemory(&message, sizeof(message));
	if (PeekMessage(&message, 0, 0, 0, PM_REMOVE))
	{
		switch (message.message)
		{
		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		case WM_QUIT:
			quit_ = true;
			break;

		default:
			; // Do nothing
		}

		TranslateMessage(&message);
		DispatchMessage(&message);
	}
}

void System::SwapState()
{
	if (nextState_ != 0)
	{
		if (currentState_ != 0)
		{
			currentState_->OnDeactivate(this);
		}
		currentState_ = nextState_;
		nextState_ = 0;
		currentState_->OnActivate(this, nextStateArgs_);
		nextStateArgs_.clear();
	}
}

void System::Update()
{
	assetLoader_->Update();
	keyboard_->Update();
	mouse_->Update(this);

	

	currentState_->OnUpdate(this);
	Sleep(1);
}

void System::Render()
{
	graphics_->BeginFrame();
	currentState_->OnRender(this);
	graphics_->EndFrame();
}
