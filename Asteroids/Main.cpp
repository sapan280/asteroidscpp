#include <Windows.h>
#include "System.h"
#include "Common.h"

int __stdcall WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow)
{
	System *systemInstance = new System(hInstance);

	systemInstance->Initialise();
	systemInstance->Test();
	systemInstance->SetNextState("BootState");
	systemInstance->Run();
	systemInstance->Terminate();
	
	SAFE_DELETE_PTR(systemInstance);

	return 0;
}
