#ifndef EXPLOSION_H_INCLUDED
#define EXPLOSION_H_INCLUDED

#include "GameEntity.h"

class Explosion : public GameEntity
{
public:
	Explosion(XMVECTOR position, int size);

	void Update(System *system);
	void Render(Graphics *graphics);

private:
	int lifeTime_;
};

#endif // EXPLOSION_H_INCLUDED
