#ifndef ASTEROID_H_INCLUDED
#define ASTEROID_H_INCLUDED

#include "GameEntity.h"

class Asteroid : public GameEntity
{
public:
	Asteroid(XMVECTOR position,
		XMVECTOR velocity,
		int size);

	void Update(System *system);
	void Render(Graphics *graphics);

	XMVECTOR GetVelocity() const;

private:

	XMFLOAT3 velocity_;
	XMFLOAT3 axis_;
	float angle_;
	float angularSpeed_;
	uint32_t normalColor;
};

#endif // ASTEROID_H_INCLUDED
