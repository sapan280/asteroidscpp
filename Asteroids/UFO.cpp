#include "UFO.h"
#include "Graphics.h"
#include "ImmediateMode.h"
#include "ImmediateModeVertex.h"


UFO::UFO(XMVECTOR pos)
	:GameEntity(4)
{
	const float UFO_SPEED = 1.0f;

	radius_ = 10;
	SetPosition(pos);
	XMStoreFloat3(&velocity_, XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f) * UFO_SPEED);
}

void UFO::Update(System * system)
{
	XMVECTOR position = GetPosition();
	position = XMVectorAdd(position, XMLoadFloat3(&velocity_));
	SetPosition(position);
}

void UFO::Render(Graphics * graphics)
{

	ImmediateModeVertex square[13] =
	{
		{-1.0f, 0.25f, 0.0f, 0xffffffff},
		{-0.50f, 0.25f, 0.0f, 0xffffffff},
		{-0.50f, 0.75f, 0.0f, 0xffffffff},
		{-0.25f, 1.0f, 0.0f, 0xffffffff},
		{0.25f,  1.0f, 0.0f, 0xffffffff},
		{0.50f,  0.75f, 0.0f, 0xffffffff},
		{0.50f,  0.25f, 0.0f, 0xffffffff},
		{1.0f,  0.25f, 0.0f, 0xffffffff},
		{1.0f,  -0.8f, 0.0f, 0xffffffff},
		{0.8f,  -1.0f, 0.0f, 0xffffffff},
		{-0.8f,  -1.0f, 0.0f, 0xffffffff},
		{-1.0f, -0.8f, 0.0f, 0xffffffff},
		{-1.0f, 0.25f, 0.0f, 0xffffffff},
	};

	XMMATRIX scaleMatrix = XMMatrixScaling(
		radius_,
		radius_,
		radius_
		);

	XMVECTOR position = GetPosition();
	XMMATRIX translationMatrix = XMMatrixTranslation(
		XMVectorGetX(position),
		XMVectorGetY(position),
		XMVectorGetZ(position));

	XMMATRIX asteroidTransform = scaleMatrix * translationMatrix;
	ImmediateMode *immediateGraphics = graphics->GetImmediateMode();

	immediateGraphics->SetModelMatrix(asteroidTransform);
	immediateGraphics->Draw(D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP,
		&square[0],
		13);
	immediateGraphics->SetModelMatrix(XMMatrixIdentity());
}
