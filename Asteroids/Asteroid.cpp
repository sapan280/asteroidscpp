#include "Asteroid.h"
#include "Graphics.h"
#include "Random.h"
#include "Maths.h"
#include "ImmediateMode.h"
#include "ImmediateModeVertex.h"

Asteroid::Asteroid(XMVECTOR position,
	XMVECTOR velocity,
	int size) : 
	GameEntity(1),
	angle_(0.0f)
{
	radius_ = size;
	SetPosition(position);

	XMStoreFloat3(&velocity_, velocity);

	axis_.x = Random::GetFloat(-1.0f, 1.0f);
	axis_.y = Random::GetFloat(-1.0f, 1.0f);
	axis_.z = Random::GetFloat(-1.0f, 1.0f);
	XMStoreFloat3(&axis_, XMVector3Normalize(XMLoadFloat3(&axis_)));

	const float MAX_ROTATION = 0.3f;
	angularSpeed_ = Random::GetFloat(-MAX_ROTATION, MAX_ROTATION);
}

void Asteroid::Update(System *system)
{
	XMVECTOR position = GetPosition();
	position = XMVectorAdd(position, XMLoadFloat3(&velocity_));
	SetPosition(position);

	angle_ = Maths::WrapModulo(angle_ + angularSpeed_, Maths::TWO_PI);

	normalColor = IsDebug() ? 0xff00ff00 : 0xffffffff;
}

void Asteroid::Render(Graphics *graphics)
{

	ImmediateModeVertex square[5] =
	{
		{-1.0f, -1.0f, 0.0f, 0xffffffff},
		{-1.0f,  1.0f, 0.0f, 0xffffffff},
		{ 1.0f,  1.0f, 0.0f, 0xffffffff},
		{ 1.0f, -1.0f, 0.0f, 0xffffffff},
		{-1.0f, -1.0f, 0.0f, 0xffffffff},
	};

	ImmediateModeVertex square1[5] =
	{
		{-1.0f, -1.0f, 0.0f, 0xff00ff00},
		{-1.0f,  1.0f, 0.0f, 0xff00ff00},
		{ 1.0f,  1.0f, 0.0f, 0xff00ff00},
		{ 1.0f, -1.0f, 0.0f, 0xff00ff00},
		{-1.0f, -1.0f, 0.0f, 0xff00ff00},
	};

	XMMATRIX scaleMatrix = XMMatrixScaling(
					radius_,
					radius_,
					radius_);

	XMMATRIX rotationMatrix = XMMatrixRotationAxis(
		XMLoadFloat3(&axis_),
		angle_);

	XMVECTOR position = GetPosition();
	XMMATRIX translationMatrix = XMMatrixTranslation(
		XMVectorGetX(position),
		XMVectorGetY(position),
		XMVectorGetZ(position));

	XMMATRIX asteroidTransform = scaleMatrix *
		rotationMatrix *
		translationMatrix;

	ImmediateMode *immediateGraphics = graphics->GetImmediateMode();

	immediateGraphics->SetModelMatrix(asteroidTransform);

	if (!IsDebug())
	{
		immediateGraphics->Draw(D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP,
			&square[0],
			5);
	}
	else
	{
		immediateGraphics->Draw(D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP,
			&square1[0],
			5);
	}
	immediateGraphics->SetModelMatrix(XMMatrixIdentity());

}

XMVECTOR Asteroid::GetVelocity() const
{
	return XMLoadFloat3(&velocity_);
}