#ifndef MAINMENU_H_INCLUDED
#define MAINMENU_H_INCLUDED

#include "GameState.h"
#include <list>
#include <string>

class MainMenu : public GameState
{
public:
	MainMenu();
	~MainMenu();

	void OnActivate(System *system, StateArgumentMap &args);
	void OnUpdate(System *system);
	void OnRender(System *system);
	void OnDeactivate(System *system);

private:
	typedef std::list<std::string> StringList;
	StringList scores_;
};

#endif // MAINMENU_H_INCLUDED
