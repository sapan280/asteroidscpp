#include "GameEntity.h"
#include "Collision.h"
#include <string>

unsigned int GameEntity::GameEntityCount = 0;

GameEntity::GameEntity(int bitsetVal) :
	id_(++GameEntity::GameEntityCount),
	position_(XMFLOAT3(0.0f, 0.0f, 0.0f)),
	collisionSystem_(0),
	collider_(0),
	markDelete(false),
	radius_(0),
	bitsetValue_(bitsetVal),
	debug_(false)
{
}

GameEntity::~GameEntity()
{
	DestroyCollider();
}

void GameEntity::Update(System *system)
{
}

void GameEntity::Render(Graphics *graphics)
{
}

XMVECTOR GameEntity::GetPosition() const
{
	return XMLoadFloat3(&position_);
}

void GameEntity::SetPosition(XMVECTOR position)
{
	XMStoreFloat3(&position_, position);

	if (HasValidCollider())
	{
		collisionSystem_->UpdateColliderPosition(collider_, position_);
	}
}

int GameEntity::GetRadius() const
{
	return radius_;
}

void GameEntity::EnableCollisions(Collision *collisionSystem, float radius)
{
	DestroyCollider();

	collisionSystem_ = collisionSystem;
	collider_ = collisionSystem_->CreateCollider(this, bitsetValue_);
	collisionSystem_->UpdateColliderPosition(collider_, position_);
	collisionSystem_->UpdateColliderRadius(collider_, radius);
}

void GameEntity::DisableCollisions()
{
	if(collisionSystem_ && collider_)
		collisionSystem_->DisableCollider(collider_);
}

void GameEntity::MarkForDelete()
{
	DisableCollisions();
	markDelete = true;
}

bool GameEntity::IsMarkedForDelete()
{
	return markDelete;
}

void GameEntity::SetDebug(bool debug)
{
	debug_ = debug;
}

bool GameEntity::IsDebug()
{
	return debug_;
}

unsigned int GameEntity::GetId()
{
	return id_;
}

Collider * GameEntity::GetCollider()
{
	return collider_;
}

bool GameEntity::HasValidCollider() const
{
	return collisionSystem_ && collider_;
}

void GameEntity::DestroyCollider()
{	
	if (!HasValidCollider())
		return;
		
	collisionSystem_->DestroyCollider(collider_);


	collisionSystem_ = 0;
	collider_ = 0;
}
