#include "Game.h"
#include "System.h"
#include "OrthoCamera.h"
#include "Background.h"
#include "Ship.h"
#include "UFO.h"
#include "Asteroid.h"
#include "Explosion.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "Random.h"
#include "Maths.h"
#include "Bullet.h"
#include "Collision.h"

#include <algorithm>
#include <ctime>

Game::Game() :
	camera_(0),
	background_(0),
	player_(0),
	collision_(0),
	bulletLifeTime_(150),
	score_(0),
	life_(0),
	spawnBulletDelayLimit_(25),
	spawnBulletDelay_(0),
	ufo_(0),
	playerTurnTick_(0),
	bulletLevel_(1),
	toggleDebugView_(false),
	collisionCheckCount_(0)
{
	camera_ = new OrthoCamera();
	camera_->SetPosition(XMFLOAT3(0.0f, 0.0f, 0.0f));
	camera_->SetFrustum(800.0f, 600.0f, -100.0f, 100.0f);
	background_ = new Background(800.0f, 600.0f);
	collision_ = new Collision();
}

Game::~Game()
{
	delete camera_;
	delete background_;
	delete player_;
	DeleteUFO();
	DeleteAllBullets();
	DeleteAllAsteroids();
	DeleteAllExplosions();
	delete collision_;
}

void Game::Update(System *system)
{
	UpdateSystems(system);
	UpdatePlayer(system);
	UpdateUFO(system);
	UpdateAsteroids(system);
	UpdateBullet(system);
	UpdateExplosions(system);
	UpdateCollisions();
	CleanUpEntities();
}

void Game::RenderBackgroundOnly(Graphics *graphics)
{
	camera_->SetAsView(graphics);
	background_->Render(graphics);
}

void Game::RenderEverything(Graphics *graphics)
{
	camera_->SetAsView(graphics);

	background_->Render(graphics);

	if(toggleDebugView_)
		collision_->RenderSpatialHash(graphics);

	if (player_)
	{
		player_->Render(graphics);

		if (toggleDebugView_)
			collision_->RenderCell(graphics, player_->GetCollider(), 0x00ff0000);
	}

	if (ufo_)
	{
		ufo_->Render(graphics);
		if (toggleDebugView_)
			collision_->RenderCell(graphics, ufo_->GetCollider(), 0x00ff0000);
	}

	for (AsteroidList::const_iterator asteroidIt = asteroids_.begin(),
		end = asteroids_.end();
		asteroidIt != end;
		++asteroidIt)
	{
		(*asteroidIt)->Render(graphics);

		if (toggleDebugView_)
			collision_->RenderCell(graphics, (*asteroidIt)->GetCollider(), 0x00ff0000);
	}

	for (BulletList::const_iterator bulletIt = bullets_.begin(),
		end = bullets_.end();
		bulletIt != end;
		++bulletIt)
	{
		(*bulletIt)->Render(graphics);

		if (toggleDebugView_)
			collision_->RenderCell(graphics, (*bulletIt)->GetCollider(), 0x00ff0000);
	}

	for (ExplosionList::const_iterator explosionIt = explosions_.begin(),
		end = explosions_.end();
		explosionIt != end;
		++explosionIt)
	{
		(*explosionIt)->Render(graphics);
	}

}

void Game::InitialiseLevel(int numAsteroids, int life)
{
	srand(time(0));

	score_ = 0;
	life_ = life;

	DeleteUFO();
	DeleteAllBullets();
	DeleteAllAsteroids();
	DeleteAllExplosions();

	SpawnPlayer();
	SpawnAsteroids(numAsteroids);

	if (rand() % 3 == 0)
	{
		float halfWidth = 800.0f * 0.5f;
		float halfHeight = 600.0f * 0.5f;

		float x = Random::GetFloat(-halfWidth, halfWidth);
		float y = Random::GetFloat(-halfHeight, halfHeight);

		SpawnUFOAt(XMVectorSet(x, y, 0.0f, 0.0f));
	}
}

bool Game::IsLevelComplete() const
{
	return (asteroids_.empty() && explosions_.empty());
}

bool Game::IsGameOver() const
{
	return (player_ == 0 && explosions_.empty() && life_ == 0);
}

void Game::DoCollision(GameEntity *a, GameEntity *b)
{
	Ship *player = static_cast<Ship *>(a == player_ ? a : (b == player_ ? b : 0));
	Bullet *bullet = static_cast<Bullet *>(IsBullet(a) ? a : (IsBullet(b) ? b : 0));
	Asteroid *asteroid = static_cast<Asteroid *>(IsAsteroid(a) ? a : (IsAsteroid(b) ? b : 0));

	if (player && player->IsMarkedForDelete())
		return;

	collisionCheckCount_++;

	if (player && asteroid)
	{
		player->MarkForDelete();
		life_--;
	}

	if (ufo_)
	{
		if (player && ufo_)
		{
			player->MarkForDelete();
			life_++;
		}

		if (bullet && ufo_)
		{
			SpawnExplosion(ufo_->GetPosition(), ufo_->GetRadius());
			ufo_->MarkForDelete();
			bullet->MarkForDelete();

			score_ += 10;
		}
	}

	if (bullet && asteroid)
	{
		SpawnExplosion(asteroid->GetPosition(), asteroid->GetRadius());

		asteroid->MarkForDelete();
		bullet->MarkForDelete();

		score_++;
	}


}

int Game::GetScore()
{
	return score_;
}

std::list<int> Game::GetScoreList()
{
	return gameScores_;
}

void Game::PrintVec(char *prefix, XMVECTOR const &vec)
{
	char s[256];
	sprintf_s(s, "%s x %f y %f z %f\n", prefix, XMVectorGetX(vec), XMVectorGetY(vec), XMVectorGetZ(vec));
	OutputDebugStringA(s);
}

bool Game::IsDebugView()
{
	return toggleDebugView_;
}

Collider * Game::GetPlayerCollider()
{
	return player_ ? player_->GetCollider() : 0;
}

std::list<Collider*> Game::GetBulletColliders()
{
	std::list<Collider*> colliders;

	for (Bullet *bullet : bullets_)
	{
		colliders.push_back(bullet->GetCollider());
	}
	return colliders;
}


int Game::GetLife()
{
	return life_;
}

int Game::GetCollisionCount()
{
	return collisionCheckCount_;
}

void Game::IncrementCollisionCount()
{
	collisionCheckCount_++;
}

int Game::GetBulletLevel()
{
	return bulletLevel_;
}

void Game::SaveScore()
{
	gameScores_.push_back(score_);
}

void Game::SpawnPlayer()
{
	DeletePlayer();
	player_ = new Ship();
	player_->EnableCollisions(collision_, player_->GetRadius());
}



void Game::DeletePlayer()
{
	delete player_;
	player_ = 0;
}

void Game::SpawnUFOAt(XMVECTOR position)
{
	ufo_ = new UFO(position);
	ufo_->EnableCollisions(collision_, 10.0f);
}

void Game::DeleteUFO()
{
	delete ufo_;
	ufo_ = 0;
}

void Game::UpdateSystems(System *system)
{
	if (system->GetKeyboard()->IsKeyPressed(VK_F1))
		toggleDebugView_ = !toggleDebugView_;
}

void Game::UpdatePlayer(System *system)
{
	if (player_ == 0)
		return;

	Keyboard *keyboard = system->GetKeyboard();
	Mouse *mouse = system->GetMouse();

	float acceleration = 0.0f;
	if (keyboard->IsKeyHeld(VK_UP) || keyboard->IsKeyHeld('W'))
	{
		acceleration = 1.0f;
	}
	else if (keyboard->IsKeyHeld(VK_DOWN) || keyboard->IsKeyHeld('S'))
	{
		acceleration = -1.0f;
	}

	float rotation = 0.0f;
	if (keyboard->IsKeyHeld(VK_RIGHT) || keyboard->IsKeyHeld('D'))
	{
		rotation = -1.0f;
	}
	else if (keyboard->IsKeyHeld(VK_LEFT) || keyboard->IsKeyHeld('A'))
	{
		rotation = 1.0f;
	}

	if (keyboard->IsKeyPressed(VK_LCONTROL))
		PrintVec("player", player_->GetPosition());

	if(keyboard->IsKeyPressed(VK_ADD))
		bulletLevel_++;

	if (keyboard->IsKeyPressed(VK_SUBTRACT))
		if(bulletLevel_ > 2)
			bulletLevel_--;

	int mouseControlled = false;
	if (mouse->IsRightMouseButtonDown())
	{
		POINT p = mouse->GetPointAfterTransform();
		XMVECTOR vec = XMVectorSet((float)mouse->GetPointAfterTransform().x, (float)mouse->GetPointAfterTransform().y, 0.0f, 0.0f);
		//PrintVec("mouse", vec);

		player_->SetTargetVector(vec);

		acceleration = 1.0f;

		mouseControlled = true;
	}
	else
	{
		mouseControlled = false;

	}

	player_->SetControlInput(acceleration, rotation, mouseControlled);

	player_->Update(system);
	WrapEntity(player_);


	if (keyboard->IsKeyHeld(VK_SPACE) || mouse->IsLeftMouseButtonDown())
	{
		if (spawnBulletDelay_ == 0)
		{
			SpawnBulletBasedOnPlayer();
			spawnBulletDelay_ = spawnBulletDelayLimit_;
		}
	}

	if (keyboard->IsKeyReleased(VK_SPACE))
	{
		spawnBulletDelay_ = 0;
	}

	if (spawnBulletDelay_ > 0)
		spawnBulletDelay_--;
}

void Game::UpdateUFO(System * system)
{
	if (ufo_)
	{
		ufo_->Update(system);
		WrapEntity(ufo_);
	}
}

void Game::UpdateAsteroids(System *system)
{
	for (AsteroidList::const_iterator asteroidIt = asteroids_.begin(),
		end = asteroids_.end();
		asteroidIt != end;
		++asteroidIt)
	{
		(*asteroidIt)->Update(system);
		WrapEntity(*asteroidIt);
	}
}

void Game::UpdateBullet(System *system)
{
	for (BulletList::const_iterator bulletIt = bullets_.begin(),
		end = bullets_.end();
		bulletIt != end;
		++bulletIt)
	{
		(*bulletIt)->Update(system);
		WrapEntity(*bulletIt);
	}
}

void Game::UpdateExplosions(System * system)
{
	for (ExplosionList::const_iterator explosionIt = explosions_.begin(),
		end = explosions_.end();
		explosionIt != end;
		++explosionIt)
	{
		(*explosionIt)->Update(system);
		WrapEntity(*explosionIt);
	}
}

void Game::WrapEntity(GameEntity *entity) const
{
	XMFLOAT3 entityPosition;
	XMStoreFloat3(&entityPosition, entity->GetPosition());
	entityPosition.x = Maths::WrapModulo(entityPosition.x, -400.0f, 400.0f);
	entityPosition.y = Maths::WrapModulo(entityPosition.y, -300.0f, 300.0f);
	entity->SetPosition(XMLoadFloat3(&entityPosition));
}

void Game::DeleteAllBullets()
{
	for (BulletList::const_iterator bulletIt = bullets_.begin(),
		end = bullets_.end();
		bulletIt != end;
		++bulletIt)
	{
		delete (*bulletIt);
	}

	bullets_.clear();
}

void Game::DeleteAllAsteroids()
{
	for (AsteroidList::const_iterator asteroidIt = asteroids_.begin(),
		end = asteroids_.end();
		asteroidIt != end;
		++asteroidIt)
	{
		delete (*asteroidIt);
	}

	asteroids_.clear();
}

void Game::DeleteAllExplosions()
{
	for (ExplosionList::const_iterator explosionIt = explosions_.begin(),
		end = explosions_.end();
		explosionIt != end;
		++explosionIt)
	{
		delete (*explosionIt);
	}

	explosions_.clear();
}

void Game::SpawnExplosion(XMVECTOR position, int lifeTime)
{
	Explosion *explosion = new Explosion(position, lifeTime);
	explosions_.push_back(explosion);

}

void Game::SpawnBulletBasedOnPlayer()
{
	XMVECTOR playerForward = player_->GetForwardVector();
	XMVECTOR bulletPosition = player_->GetPosition() + playerForward * 10.0f;

	if (bulletLevel_ == 1)
	{
		SpawnBullet(bulletPosition, playerForward, bulletLifeTime_);
	}
	else
	{
		float angle = atan2f(XMVectorGetY(playerForward), XMVectorGetX(playerForward)) - Maths::PI / 2;
		int size = bulletLevel_;

		float rangeInDegree = 10.0;
		float rangeInRadians = rangeInDegree * Maths::PI / 180;

			for (int i = 0; i < size; i++)
			{
				float netAngle = angle + (rangeInRadians / (size - 1) * (i - (0.5f * (size - 1))));
				XMMATRIX rotationMatrix = XMMatrixRotationZ(netAngle);
				XMVECTOR newForward = XMVector3TransformNormal(XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f), rotationMatrix);

				SpawnBullet(bulletPosition, newForward, bulletLifeTime_);
			}

	}
}

void Game::SpawnBullet(XMVECTOR position, XMVECTOR direction, int lifeTime)
{
	Bullet *bullet = new Bullet(position, direction, lifeTime);
	bullet->EnableCollisions(collision_, bullet->GetRadius());
	bullets_.push_back(bullet);

}

void Game::DeleteBullet(Bullet *bullet)
{
	delete bullet;
	bullet = 0;
}

void Game::DeleteExplosion(Explosion* explosion)
{
	delete explosion;
	explosion = 0;
}

void Game::SpawnAsteroids(int numAsteroids)
{
	float halfWidth = 800.0f * 0.5f;
	float halfHeight = 600.0f * 0.5f;
	for (int i = 0; i < numAsteroids; i++)
	{
		float x = Random::GetFloat(-halfWidth, halfWidth);
		float y = Random::GetFloat(-halfHeight, halfHeight);
		XMVECTOR position = XMVectorSet(x, y, 0.0f, 0.0f);
		SpawnAsteroidAt(position, 15);
	}
}

void Game::SpawnAsteroidAt(XMVECTOR position, int size)
{
	const float MAX_ASTEROID_SPEED = 1.0f;

	float angle = Random::GetFloat(Maths::TWO_PI);
	XMMATRIX randomRotation = XMMatrixRotationZ(angle);
	XMVECTOR velocity = XMVectorSet(0.0f, Random::GetFloat(MAX_ASTEROID_SPEED), 0.0f, 0.0f);
	velocity = XMVector3TransformNormal(velocity, randomRotation);

	Asteroid *asteroid = new Asteroid(position, velocity, size);
	asteroid->EnableCollisions(collision_, asteroid->GetRadius());
	asteroids_.push_back(asteroid);
}

bool Game::IsBullet(GameEntity *entity) const
{
	return (std::find(bullets_.begin(),
		bullets_.end(), entity) != bullets_.end());
}

bool Game::IsAsteroid(GameEntity *entity) const
{
	return (std::find(asteroids_.begin(),
		asteroids_.end(), entity) != asteroids_.end());
}

void Game::AsteroidHit(Asteroid *asteroid)
{
	int oldSize = asteroid->GetRadius();
	if (oldSize > 10)
	{
		int smallerSize = oldSize * 0.5;
		XMVECTOR position = asteroid->GetPosition();
		SpawnAsteroidAt(position, smallerSize);
		SpawnAsteroidAt(position, smallerSize);
	}
	DeleteAsteroid(asteroid);
}

void Game::DeleteAsteroid(Asteroid *asteroid)
{
	//asteroids_.remove(asteroid);	//this line is commented as its safe to erase the astoroids from list using erase function inside a loop
	delete asteroid;
}

void Game::UpdateCollisions()
{
	collisionCheckCount_ = 0;

	collision_->DoCollisions(this);	

	char s[256];
	sprintf_s(s, "collision cound %d\n", collisionCheckCount_);
	OutputDebugStringA(s);
}

void Game::CleanUpEntities()
{
	if (player_ && player_->IsMarkedForDelete())
	{
		DeletePlayer();

		if (life_ > 0)
			SpawnPlayer();
	}

	if (ufo_ && ufo_->IsMarkedForDelete())
	{
		DeleteUFO();
	}


	auto bulletIt = bullets_.begin();
	while (bulletIt != bullets_.end())
	{
		auto curr = bulletIt++;
		if (*curr && 1 && (*curr)->IsMarkedForDelete())
		{
			DeleteBullet(*curr);
			bullets_.erase(curr);
		}
	}

	auto it = asteroids_.begin();
	while (it != asteroids_.end())
	{
		auto curr = it++;
		if (*curr && 1 && (*curr)->IsMarkedForDelete())
		{
			AsteroidHit(*curr);
			asteroids_.erase(curr);
		}
	}

	auto explosionIt = explosions_.begin();
	while (explosionIt != explosions_.end())
	{
		auto curr = explosionIt++;
		if (*curr && 1 && (*curr)->IsMarkedForDelete())
		{
			DeleteExplosion(*curr);
			explosions_.erase(curr);
		}
	}
}