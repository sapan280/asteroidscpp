#ifndef UFO_H_INCLUDED
#define UFO_H_INCLUDED

#include "GameEntity.h"

class UFO : public GameEntity
{
public:
	UFO(XMVECTOR position);
	
	void Update(System *system);
	void Render(Graphics *graphics);

private:
	XMFLOAT3 velocity_;
};


#endif // UFO_H_INCLUDED
