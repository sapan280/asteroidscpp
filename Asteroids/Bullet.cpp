#include "Bullet.h"
#include "Graphics.h"
#include "ImmediateMode.h"
#include "ImmediateModeVertex.h"

Bullet::Bullet(XMVECTOR position, XMVECTOR direction, int lifeTime)
	:GameEntity(2)
{
	lifeTime_ = lifeTime;
	const float BULLET_SPEED = 4.0f;

	SetPosition(position);
	XMVECTOR normalisedDirection = XMVector3Normalize(direction);
	XMStoreFloat3(&velocity_, normalisedDirection * BULLET_SPEED);

	radius_ = 3;
}

void Bullet::Update(System *system)
{
	XMVECTOR position = GetPosition();
	position = XMVectorAdd(position, XMLoadFloat3(&velocity_));
	SetPosition(position);

	lifeTime_--;
	
	if (lifeTime_ <= 0)
		MarkForDelete();
}

void Bullet::Render(Graphics *graphics)
{

	ImmediateModeVertex square[5] =
	{
		{-radius_, -radius_, 0.0f, 0x000fffff},
		{-radius_,  radius_, 0.0f, 0x000fffff},
		{ radius_,  radius_, 0.0f, 0x000fffff},
		{ radius_, -radius_, 0.0f, 0x000fffff},
		{-radius_, -radius_, 0.0f, 0x000fffff},
	};

	XMVECTOR position = GetPosition();
	XMMATRIX translationMatrix = XMMatrixTranslation(
		XMVectorGetX(position),
		XMVectorGetY(position),
		XMVectorGetZ(position));

	ImmediateMode *immediateGraphics = graphics->GetImmediateMode();

	immediateGraphics->SetModelMatrix(translationMatrix);
	immediateGraphics->Draw(D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP,
		&square[0],
		5);
	immediateGraphics->SetModelMatrix(XMMatrixIdentity());
}
