#ifndef MOUSE_H_INCLUDED
#define MOUSE_H_INCLUDED

#include <Windows.h>

class System;

class Mouse
{
public:
	Mouse();
	~Mouse();

	void Update(System *system);
	POINT GetPointAfterTransform();
	bool IsLeftMouseButtonDown();
	bool IsRightMouseButtonDown();

private:
	POINT p;
};

#endif // MOUSE_H_INCLUDED