#ifndef BULLET_H_INCLUDED
#define BULLET_H_INCLUDED

#include "GameEntity.h"

class Bullet : public GameEntity
{
public:
	Bullet(XMVECTOR position,
		XMVECTOR direction, int lifeTime);

	void Update(System *system);
	void Render(Graphics *graphics);

private:

	XMFLOAT3 velocity_;
	int lifeTime_;
};

#endif // BULLET_H_INCLUDED
