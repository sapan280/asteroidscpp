#include "Collision.h"
#include "Collider.h"
#include "Game.h"
#include "GameEntity.h"
#include "SpatialHash.h"
#include "Graphics.h"
#include "Maths.h"
#include "Common.h"

#include <functional>
#include <Windows.h>

Collision::Collision()
{
	spatialHash_ = new SpatialHash(800, 600, 50);
}

Collision::~Collision()
{
	for (ColliderList::iterator colliderIt = colliders_.begin(), end = colliders_.end();
		colliderIt != end;
		++colliderIt)
	{
		delete *colliderIt;
	}
}

Collider *Collision::CreateCollider(GameEntity *entity, int setvalue)
{
	Collider *collider = new Collider();

	collider->position = XMFLOAT3(0.0f, 0.0f, 0.0f);
	collider->radius = (float)entity->GetRadius();
	collider->entity = entity;
	collider->enabled = true;
	collider->bitset.set(setvalue);
	colliders_.push_back(collider);

	return collider;
}

void Collision::DestroyCollider(Collider *collider)
{
	colliders_.remove_if(std::bind1st((std::equal_to<Collider *>()), collider));
	delete collider;
}

void Collision::UpdateColliderPosition(Collider *collider, const XMFLOAT3 &position)
{
	collider->position = position;
}

void Collision::UpdateColliderRadius(Collider *collider, float radius)
{
	collider->radius = radius;
}

void Collision::EnableCollider(Collider *collider)
{
	collider->enabled = true;
}

void Collision::DisableCollider(Collider *collider)
{
	collider->enabled = false;
}

void Collision::DoCollisions(Game *game)
{
	int collisionTestCount = 0;

#ifdef ENABLE_SPATIAL

	spatialHash_->DeleteAll();

	for (Collider *collider : colliders_)
	{
		spatialHash_->InsertEntity(collider);
	}

#ifdef ENABLE_SPATIAL_CUSTOM

	//check collision of players with others
	Collider *colliderA = game->GetPlayerCollider();
	if (colliderA)
	{
		CheckCollisionsWithNeighbours(colliderA, game);
	}

	//check collision of bullets with others
	std::list<Collider*> bullets = game->GetBulletColliders();
	{
		for (Collider *bullet : bullets)
		{
			CheckCollisionsWithNeighbours(bullet, game);
		}
	}

#else

	for (ColliderList::const_iterator colliderAIt = colliders_.begin(), end = colliders_.end();
		colliderAIt != end;
		++colliderAIt)
	{
		Collider *colliderA = *colliderAIt;


	}

#endif // ENABLE_SPATIAL_CUSTOM

#else

	for (ColliderList::const_iterator colliderAIt = colliders_.begin(), end = colliders_.end();
		colliderAIt != end;
		++colliderAIt)
	{
		ColliderList::const_iterator colliderBIt = colliderAIt;
		for (++colliderBIt; colliderBIt != end; ++colliderBIt)
		{
			Collider *colliderA = *colliderAIt;
			Collider *colliderB = *colliderBIt;

			game->IncrementCollisionCount();

			if (CollisionTest(colliderA, colliderB))
			{
				game->DoCollision(colliderA->entity, colliderB->entity);
			}
		}
	}

#endif // ENABLE_SPATIAL

}

void Collision::RenderSpatialHash(Graphics *graphics)
{
	spatialHash_->Render(graphics);
}

void Collision::RenderCell(Graphics * graphics, Collider *collider, uint32_t color)
{

	std::list<CellLoc> cells = spatialHash_->GetIdForObj(collider);

	for (std::list<CellLoc>::const_iterator cellIt = cells.begin(),
		end = cells.end();
		cellIt != end;
		++cellIt)
	{
		CellLoc v = (*cellIt);

		//char s[256];
		//sprintf_s(s, "get -> [ x %d y %d ]\n", v.x, v.y);
		//OutputDebugStringA(s);


		//x = Maths::WrapModulo(x, -8, 8);
		//y = Maths::WrapModulo(y, -8, 8);


		spatialHash_->RenderCell(graphics->GetImmediateMode(), v.x, v.y, color);
	}
}

bool Collision::CollisionTest(Collider *a, Collider *b)
{
	if (a->enabled == false)
		return false;
	if (b->enabled == false)
		return false;

	XMVECTOR diff = XMVectorSubtract(XMLoadFloat3(&a->position), XMLoadFloat3(&b->position));
	float distance = XMVectorGetX(XMVector3Length(diff));
	if (distance < (a->radius + b->radius))
	{
		return true;
	}

	return false;
}

void Collision::CheckCollisionsWithNeighbours(Collider * colliderA, Game *game)
{
	if (!colliderA->enabled)
		return;

	std::list<Collider*> clist = spatialHash_->FetchNearbyEntity(colliderA, colliderA->entity->GetId());

	for (auto colliderB : clist)
	{
		if (!colliderB->enabled || colliderA->bitset == colliderB->bitset)	//instead of simple bitset equality check, implement collision filters
			continue;

		game->IncrementCollisionCount();

		if (CollisionTest(colliderA, colliderB))
		{
			game->DoCollision(colliderA->entity, colliderB->entity);
		}
	}
}
