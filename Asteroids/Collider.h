#ifndef COLLIDER_H_INCLUDED
#define COLLIDER_H_INCLUDED

#include <DirectXMath.h>
#include <bitset>
#include <list>

using namespace DirectX;

class GameEntity;

class Collider
{
	friend class Collision;
	friend class SpatialHash;
private:
	std::list<int> spatialId;
	XMFLOAT3 position;
	float radius;
	GameEntity *entity;
	bool enabled;
	std::bitset<8> bitset;
};

#endif // COLLIDER_H_INCLUDED
