#ifndef COMMON_H_
#define COMMON_H_

#define SAFE_DELETE_PTR(a) if(a) delete a; a = 0;

#define ENABLE_SPATIAL
#define ENABLE_SPATIAL_CUSTOM

#endif // !COMMON_H_